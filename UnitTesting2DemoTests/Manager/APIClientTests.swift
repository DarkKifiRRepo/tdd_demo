//
//  APIClientTests.swift
//  UnitTesting2DemoTests
//
//  Created by Александр Евсеев on 06/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest
@testable import UnitTesting2Demo

class APIClientTests: XCTestCase {

    var sut: APIClient!
    var mockURLSession: MockURLSession!
    
    override func setUp() {
        
        sut = APIClient()
        mockURLSession = MockURLSession(data: nil, urlResponse: nil, responseError: nil)
        sut.urlSession = mockURLSession
        
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func userLogin() {
        let completionHandler = {(token: String?, error: Error?) in }
        sut.login(withName: "name", password: "%qwerty", completionHandler: completionHandler)
    }
    
    func testLoginUsesCorrectHost() {
        userLogin()
        XCTAssertEqual(mockURLSession.urlComponents?.host, "todoapp.com")
    }
    
    func testLoginUsesCorrectSecondComponent() {
        userLogin()
        XCTAssertEqual(mockURLSession.urlComponents?.path, "/login")
    }
    
    func testLoginUsesExpectedQueryParameters() {
        userLogin()
//        guard let queryItems = mockURLSession.urlComponents?.queryItems else {
//            XCTFail()
//            return
//        }
        
        let urlQueryName = URLQueryItem(name: "name", value: "name")
        let urlQueryPassword = URLQueryItem(name: "password", value: "%qwerty")
        XCTAssertTrue((mockURLSession.urlComponents?.queryItems?.contains(urlQueryName))!)
        XCTAssertTrue((mockURLSession.urlComponents?.queryItems?.contains(urlQueryPassword))!)
//        XCTAssertEqual(mockURLSession.urlComponents?.percentEncodedQuery, "name=name&password=%25qwerty")
    }
    
    // token -> Data -> completionHadnler -> DataTask -> URLSession
    func testSuccsesfulLoginCreatesToken() {
        let jsonDataStub = "{\"token\": \"tokenString\"}".data(using: .utf8)
        mockURLSession = MockURLSession(data: jsonDataStub, urlResponse: nil, responseError: nil)
        sut.urlSession = mockURLSession
        let tokenExpectation = expectation(description: "token expectation")
        
        var caughtToken: String?
        sut.login(withName: "login", password: "qwerty") { token, _ in
            caughtToken = token
            tokenExpectation.fulfill()
        }
        waitForExpectations(timeout: 1) { _ in
            XCTAssertEqual(caughtToken, "tokenString")
        }
    }
    
    func testInvalidLoginCausesError() {
        mockURLSession = MockURLSession(data: Data(), urlResponse: nil, responseError: nil)
        sut.urlSession = mockURLSession
        let errorExpectation = expectation(description: "error expectation")
        
        var caughtError: Error?
        sut.login(withName: "login", password: "qwerty") { _, error in
            caughtError = error
            errorExpectation.fulfill()
        }
        waitForExpectations(timeout: 1) { _ in
            XCTAssertNotNil(caughtError)
        }
    }
    
    func testLoginWhenDataIsNilCausesError() {
        mockURLSession = MockURLSession(data: nil, urlResponse: nil, responseError: nil)
        sut.urlSession = mockURLSession
        let errorExpectation = expectation(description: "error expectation")
        
        var caughtError: Error?
        sut.login(withName: "login", password: "qwerty") { _, error in
            caughtError = error
            errorExpectation.fulfill()
        }
        waitForExpectations(timeout: 1) { _ in
            XCTAssertNotNil(caughtError)
        }
    }
    
    func testLoginWhenResponceErrorCausesError() {
        let jsonDataStub = "{\"token\": \"tokenString\"}".data(using: .utf8)
        let responceError = NSError(domain: "Server error", code: 404, userInfo: nil)
        mockURLSession = MockURLSession(data: jsonDataStub, urlResponse: nil, responseError: responceError)
        sut.urlSession = mockURLSession
        let errorResponceExpectation = expectation(description: "responce error expectation")
        
        var caughtResponceError: Error?
        sut.login(withName: "login", password: "qwerty") { _, error in
            caughtResponceError = error
            errorResponceExpectation.fulfill()
        }
        waitForExpectations(timeout: 1) { _ in
            XCTAssertNotNil(caughtResponceError)
        }
    }
}

extension APIClientTests {
    class MockURLSession: URLSessionProtocol {
        var url: URL?
        
        private let mockDataTask: MockURLSessionDataTask
        
        var urlComponents: URLComponents? {
            guard let url = url else {
                return nil
            }
            return URLComponents(url: url, resolvingAgainstBaseURL: true)
        }
        
        init(data: Data?, urlResponse: URLResponse?, responseError: Error?) {
            mockDataTask = MockURLSessionDataTask(data: data, urlResponse: urlResponse, responseError: responseError)
        }
        
        func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
            self.url = url
//            return URLSession.shared.dataTask(with: url)
            mockDataTask.completionhandler = completionHandler
            return mockDataTask
        }
    }
    
    class MockURLSessionDataTask: URLSessionDataTask {
        
        private let data: Data?
        private let urlResponse: URLResponse?
        private let responseError: Error?
        
        typealias CompletionHandler = (Data?, URLResponse?, Error?) -> Void
        var completionhandler: CompletionHandler?
        
        init(data: Data?, urlResponse: URLResponse?, responseError: Error?) {
            self.data = data
            self.urlResponse = urlResponse
            self.responseError = responseError
        }
        
        override func resume() {
            DispatchQueue.main.async {
                self.completionhandler? (
                    self.data,
                    self.urlResponse,
                    self.responseError
                )
            }
        }
    }
}
