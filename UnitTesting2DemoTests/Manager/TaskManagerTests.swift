//
//  TaskManagerTests.swift
//  UnitTesting2DemoTests
//
//  Created by Александр Евсеев on 04/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest
@testable import UnitTesting2Demo

class TaskManagerTests: XCTestCase {
    
    var sut: TaskManager!
    
    override func setUp() {
        super.setUp()
        
        sut = TaskManager()
    }

    override func tearDown() {
        sut.removeAll()
        sut = nil
        
        super.tearDown()
    }

    func testInitTaskManagerWithZeroTasks() {
        
        XCTAssertEqual(sut.tasksCount, 0)
    }
    
    func testInitTaskManagerWithZeroDoneTasks() {
        
        XCTAssertEqual(sut.doneTasksCount, 0)
    }
    
    func testAddTaskIncrementTaskCount() {
        let task = Task(title: "Foo")
        
        sut.add(task: task)
        
        XCTAssertEqual(sut.tasksCount, 1)
    }
    
    func testTaskAtIndexIsAddedTask() {
        let task = Task(title: "Foo")
        sut.add(task: task)
        
        let returnedTask = sut.task(at: 0)
        
        XCTAssertEqual(returnedTask, task)
    }
    
    func testCheckDoneTaskAtIndexChangesCount() {
        let task = Task(title: "Foo")
        sut.add(task: task)
        
        sut.checkTask(at: 0)
        
        XCTAssertEqual(sut.tasksCount, 0)
        XCTAssertEqual(sut.doneTasksCount, 1)
    }
    
    func testCheckedTaskRemovedFromTasks() {
        let firstTask = Task(title: "Foo")
        let secondTask = Task(title: "Bar")
        
        sut.add(task: firstTask)
        sut.add(task: secondTask)
        sut.checkTask(at: 0)
        
        XCTAssertNotEqual(firstTask, sut.task(at: 0))
    }
    
    func testDoneTaskAtReturnsCheckedTask() {
        let task = Task(title: "Foo")
        sut.add(task: task)
        
        sut.checkTask(at: 0)
        let returnedTask = sut.doneTask(at: 0)
        
        XCTAssertEqual(task, returnedTask)
    }
    
    func testRemoveAllCountsShouldBeZero() {
        sut.add(task: Task(title: "Foo"))
        sut.add(task: Task(title: "Bar"))
        sut.add(task: Task(title: "Baz"))
        
        sut.checkTask(at: 1)
        sut.removeAll()
        
        XCTAssertEqual(sut.tasksCount, 0)
        XCTAssertTrue(sut.doneTasksCount == 0)
    }
    
    func testAddTwoEqualTasksNotShoulgAdded() {
        sut.add(task: Task(title: "Foo"))
        sut.add(task: Task(title: "Foo"))
        
        XCTAssertEqual(sut.tasksCount, 1)
    }
    
    func testWhenTaskManagerRecreatedSavedTasksShouldBeEqual() {
        var taskManager: TaskManager! = TaskManager()
        let task1 = Task(title: "Foo")
        let task2 = Task(title: "Bar")
        
        taskManager.add(task: task1)
        taskManager.add(task: task2)
        
        NotificationCenter.default.post(name: UIApplication.willResignActiveNotification, object: nil)
        taskManager = nil
        taskManager = TaskManager()
        
        XCTAssertEqual(taskManager.tasksCount, 2)
        XCTAssertEqual(taskManager.task(at: 0), task1)
        XCTAssertEqual(taskManager.task(at: 1), task2)
    }
    
    func testTaskHaveAndChangeDoneValue() {
        let taskManager = TaskManager()
        let task = Task(title: "Foo")
        
        taskManager.add(task: task)
        let checkedUndoneTask = taskManager.task(at: 0)
        XCTAssertEqual(checkedUndoneTask.isDone, false)
        
        taskManager.checkTask(at: 0)
        let checkedDoneTask = taskManager.doneTask(at: 0)
        XCTAssertEqual(checkedDoneTask.isDone, true)
    }
}
