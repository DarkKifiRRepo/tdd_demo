//
//  TaskCellTests.swift
//  UnitTesting2DemoTests
//
//  Created by Александр Евсеев on 05/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest
@testable import UnitTesting2Demo

class TaskCellTests: XCTestCase {
    
    var cell: TaskCell!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: String(describing: TaskListViewController.self)) as! TaskListViewController
        controller.loadViewIfNeeded()
        
        let tableView = controller.tableView
        let dataSource = FakeDataSource()
        tableView?.dataSource = dataSource
        
        cell = tableView?.dequeueReusableCell(withIdentifier: String(describing: TaskCell.self), for: IndexPath(row: 0, section: 0)) as? TaskCell
    }

    override func tearDown() {
        
        super.tearDown()
    }
    
    func testCellHaveTitleLabel() {
        
        XCTAssertNotNil(cell.titleLabel)
    }
    
    func testCellHaveTitleLabelInContentView() {
        
        XCTAssertTrue(cell.titleLabel.isDescendant(of: cell.contentView))
    }
    
    func testCellHaveLocationLabel() {
        
        XCTAssertNotNil(cell.locationLabel)
    }
    
    func testCellHaveLocationLabelInContentView() {
        
        XCTAssertTrue(cell.locationLabel.isDescendant(of: cell.contentView))
    }
    
    func testCellHaveDateLabel() {
        
        XCTAssertNotNil(cell.dateLabel)
    }
    
    func testCellHaveDateLabelInContentView() {
        
        XCTAssertTrue(cell.dateLabel.isDescendant(of: cell.contentView))
    }
    
    func testCellConfigureTitle() {
        let task = Task(title: "Foo")
        
        cell.configure(with: task)
        
        XCTAssertEqual(cell.titleLabel.text, task.title)
    }
    
    func testCellConfigureDate() {
        let task = Task(title: "Foo")
        
        cell.configure(with: task)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yy"
        let date = task.date
        let dateSting = dateFormatter.string(from: date)
        
        XCTAssertEqual(cell.dateLabel.text, dateSting)
    }
    
    func testCellConfigureLocation() {
        let task = Task(title: "Foo",
                        description: "Bar",
                        location: Location(named: "Baz")
        )
        
        cell.configure(with: task)
        
        XCTAssertEqual(cell.locationLabel.text, task.location?.name)
    }
    
    func testCellConfigureNilLocation() {
        let task = Task(title: "Foo",
                        description: "Bar",
                        location: nil
        )
        
        cell.configure(with: task)
        
        XCTAssertEqual(cell.locationLabel.text, task.location?.name)
    }
    
    func configureCellForTestWithTask() {
        let task = Task(title: "Foo")
        cell.configure(with: task, done: true)
    }
    
    func testDoneTaskShouldStrikeThrough() {
        configureCellForTestWithTask()
        
        let attrubitedString = NSAttributedString(string: "Foo", attributes: [NSAttributedString.Key.strikethroughStyle : NSUnderlineStyle.single.rawValue])
        
        XCTAssertEqual(cell.titleLabel.attributedText, attrubitedString)
    }
    
    func testDoneTaskDateLabelTextEqualsEmptyString() {
        configureCellForTestWithTask()
        
        XCTAssertEqual(cell.dateLabel.text, "")
    }
    
    func testDoneTaskLocationLabelTextEqualsEmptyString() {
        configureCellForTestWithTask()
        
        XCTAssertEqual(cell.locationLabel.text, "")
    }
}

extension TaskCellTests {
    class FakeDataSource: NSObject, UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            return UITableViewCell()
        }
    }
}
