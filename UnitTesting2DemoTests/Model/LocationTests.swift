//
//  LocationTests.swift
//  UnitTesting2DemoTests
//
//  Created by Александр Евсеев on 04/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest
@testable import UnitTesting2Demo
import CoreLocation

class LocationTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
    }
    
    override func tearDown() {
        
        super.tearDown()
    }
    
    func testInitLocationWithName() {
        let location = Location(named: "Foo")
        
        XCTAssertEqual(location.name, "Foo")
    }
    
    func testInitWithCoordinates() {
        let coordinate = CLLocationCoordinate2D(
            latitude: 1,
            longitude: 2)
        let location = Location(named: "Foo", coordinates: coordinate)
        
        XCTAssertEqual(coordinate.latitude, location.coordinate?.latitude)
        XCTAssertEqual(coordinate.longitude, location.coordinate?.longitude)
    }
    
    func testCanBeCreatedFromPlistDictonary() {
        let location = Location(named: "Foo", coordinates: CLLocationCoordinate2D(latitude: 10, longitude: 10))
        let dictionary: [String: Any] = [ "name": "Foo",
                                        "latitude": 10.0,
                                        "longitude": 10.0]
        let createdLocation = Location(dict: dictionary)
        
        XCTAssertEqual(location, createdLocation)
    }
    
    func testCanBeSerilizedIntoDictionary() {
        let location = Location(named: "Foo", coordinates: CLLocationCoordinate2D(latitude: 10, longitude: 10))
        let generatedLocation = Location(dict: location.dict)
        
        XCTAssertEqual(location, generatedLocation)
    }
}
