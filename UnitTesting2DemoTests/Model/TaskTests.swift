//
//  TaskTests.swift
//  UnitTesting2DemoTests
//
//  Created by Александр Евсеев on 04/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest
@testable import UnitTesting2Demo

class TaskTests: XCTestCase {

    func testInitTaskWithTitle() {
        let task = Task(title: "Foo")
        
        XCTAssertNotNil(task)
    }
    
    func testInitTaskWithTitleAndDiscription() {
        let task = Task(title: "Foo", description: "Bar")
        
        XCTAssertNotNil(task)
    }
    
    func testWhenGivenTitleSetup() {
        let task = Task(title: "Foo")
        
        XCTAssertEqual(task.title, "Foo")
    }
    
    func testWhenGivenDescriptionSetup() {
        let task = Task(title: "Foo", description: "Bar")
        
        XCTAssertEqual(task.description, "Bar")
    }
    
    func testInitTaskWithDate() {
        let task = Task(title: "Foo")
        
        XCTAssertNotNil(task.date)
    }
    
    func testWhenGivenLocationSetup() {
        let location = Location(named: "Foo")
        
        let task = Task(title: "bas",
                        description: "bar",
                        location: location)
        XCTAssertEqual(task.location, location)
    }
    
    func testCanBeCreatedFromPlistDictionary() {
        let location = Location(named: "Baz")
        let date = Date(timeIntervalSince1970: 146000)
        let task = Task(title: "Foo", description: "Bar", date: date, location: location)
        
        let locationDictionary: [String: Any] = [ "name" : "Baz" ]
        
        let dictionary: [String: Any] = [ "title":"Foo",
                                          "description":"Bar",
                                          "date": date,
                                          "location": locationDictionary ]
        
        let createdTask = Task(dict: dictionary)
        
        XCTAssertEqual(task, createdTask)
    }
    
    func testCanBeSerializedIntoDictionary() {
        let location = Location(named: "Baz")
        let date = Date(timeIntervalSince1970: 146000)
        let task = Task(title: "Foo", description: "Bar", date: date, location: location)
        
        let generatedTask = Task(dict: task.dict)
        
        XCTAssertEqual(generatedTask, task)
    }
}
