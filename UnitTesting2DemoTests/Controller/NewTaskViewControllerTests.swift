//
//  NewTaskViewControllerTests.swift
//  UnitTesting2DemoTests
//
//  Created by Александр Евсеев on 05/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest
import CoreLocation
@testable import UnitTesting2Demo

class NewTaskViewControllerTests: XCTestCase {

    var sut: NewTaskViewController!
    var placemark: MockCLPlacemark!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyboard.instantiateViewController(withIdentifier: String(describing: NewTaskViewController.self)) as? NewTaskViewController
        sut.loadViewIfNeeded()
    }

    override func tearDown() {
        
        super.tearDown()
    }
    
    func testHasTitleTextField() {
        XCTAssertTrue(sut.titleTextField.isDescendant(of: sut.view))
    }
    
    func testHasDescriptionTextField() {
        XCTAssertTrue(sut.descriptionTextField.isDescendant(of: sut.view))
    }
    
    func testHasLocationTextField() {
        XCTAssertTrue(sut.locationTextField.isDescendant(of: sut.view))
    }
    
    func testHasAddressTextField() {
        XCTAssertTrue(sut.addressTextField.isDescendant(of: sut.view))
    }
    
    func testHasDateTextField() {
        XCTAssertTrue(sut.dateTextField.isDescendant(of: sut.view))
    }
    
    func testHasSaveCancelButtons() {
        XCTAssertTrue(sut.saveButton.isDescendant(of: sut.view))
        XCTAssertTrue(sut.cancelButton.isDescendant(of: sut.view))
    }
    
    func testSaveUsesGeocoderToConvertCoordinatesToAddress() {
        let df = DateFormatter()
        df.dateFormat = "dd.MM.yy"
        
        sut.titleTextField.text = "Foo"
        sut.locationTextField.text = "Bar"
        sut.descriptionTextField.text = "Baz"
        sut.dateTextField.text = "01.01.19"
        sut.addressTextField.text = "Уфа"
        
        sut.taskManager = TaskManager()
        let mockGeocoder = MockCLGeocoder()
        sut.geocoder = mockGeocoder
        sut.save()
        
        let coordinate = CLLocationCoordinate2D(latitude: 54.7373058, longitude: 55.9722491)
        let location = Location(named: "Bar", coordinates: coordinate)
        let generatedTask = Task(title: "Foo", description: "Baz", date: df.date(from: "01.01.19"), location: location)
        
        placemark = MockCLPlacemark()
        placemark.mockCoordinate = coordinate
        mockGeocoder.completionHandler?([placemark], nil)
        
        let task = sut.taskManager.task(at: 0)
        
        XCTAssertEqual(task, generatedTask)
    }
    
    func testSaveButtonHasSaveMethod() {
        let saveButton = sut.saveButton
        
        guard let actions = saveButton?.actions(forTarget: sut, forControlEvent: .touchUpInside) else {
            XCTFail()
            return
        }
        XCTAssertTrue(actions.contains("saveButtonPressed:"))
    }
    
    func testGeocoderFetchCorrectCoordinate() {
        let geocoderAnswer = expectation(description: "GeocoderAnswer") // need for client-server architecture
        let addressString = "Уфа"
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(addressString) { (placemarks, error) in
            let placemark = placemarks?.first
            let location = placemark?.location
            
            guard
                let latitude = location?.coordinate.latitude,
                let longitude = location?.coordinate.longitude else {
                    XCTFail()
                    return
            }
            XCTAssertEqual(54.7373058, latitude)
            XCTAssertEqual(55.9722491, longitude)
            geocoderAnswer.fulfill() // need for async testing
        }
        waitForExpectations(timeout: 5, handler: nil) // generate timeout for expectation
    }
    
    func testSaveDismissesNewTaskViewController() {
        // given
        let mockNewTaskVC = MockNewTaskVC()
        mockNewTaskVC.titleTextField = UITextField()
        mockNewTaskVC.titleTextField.text = "Foo"
        mockNewTaskVC.descriptionTextField = UITextField()
        mockNewTaskVC.descriptionTextField.text = "Bar"
        mockNewTaskVC.addressTextField = UITextField()
        mockNewTaskVC.addressTextField.text = "Уфа"
        mockNewTaskVC.dateTextField = UITextField()
        mockNewTaskVC.dateTextField.text = "01.01.19"
        mockNewTaskVC.locationTextField = UITextField()
        mockNewTaskVC.locationTextField.text = "Baz"
        
        // when
        
        mockNewTaskVC.save()
        
        // then
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
            XCTAssertTrue(mockNewTaskVC.dismissPerform)
        }
    }
    
    func testCancelCallCancelButtonPressedMethod() {
        let cancelButton = sut.cancelButton
        
        guard let actions = cancelButton?.actions(forTarget: sut, forControlEvent: .touchUpInside) else {
            XCTFail()
            return
        }
        XCTAssertTrue(actions.contains("cancelButtonPressed:"))
    }
    
    func testCancelButtonPerformDismissNewTaskViewController() {
        
        let cancelButton = sut.cancelButton
        
        cancelButton?.sendActions(for: .touchUpInside)
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            XCTAssertTrue(!(topController is NewTaskViewController))
        }
        
        
    }
}

extension NewTaskViewControllerTests {
    class MockCLGeocoder: CLGeocoder {
        var completionHandler: CLGeocodeCompletionHandler?
        
        override func geocodeAddressString(_ addressString: String, completionHandler: @escaping CLGeocodeCompletionHandler) {
            self.completionHandler = completionHandler
        }
    }
    
    class MockCLPlacemark: CLPlacemark {
        var mockCoordinate: CLLocationCoordinate2D!
        
        override var location: CLLocation? {
            return CLLocation(latitude: mockCoordinate.latitude, longitude: mockCoordinate.longitude)
        }
    }
}

extension NewTaskViewControllerTests {
    class MockNewTaskVC: NewTaskViewController {
        var dismissPerform = false
        
        override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
            dismissPerform = true
        }
    }
}
