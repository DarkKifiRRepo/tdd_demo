//
//  TaskListViewControllerTests.swift
//  UnitTesting2DemoTests
//
//  Created by Александр Евсеев on 04/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest
@testable import UnitTesting2Demo

class TaskListViewControllerTests: XCTestCase {
    
    var sut: TaskListViewController!
    
    override func setUp() {
        super.setUp()
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: String(describing: TaskListViewController.self))
        sut = vc as? TaskListViewController
        
        sut.loadViewIfNeeded()
    }

    override func tearDown() {
        
        super.tearDown()
    }
    
    func testWhenViewIsLoadedTableViewNotNil() {
        
        XCTAssertNotNil(sut.tableView)
    }
    
    func testWhenVewIsLoadedDataProviderIsNotNil() {

        XCTAssertNotNil(sut.dataProvider)
    }
    
    func testWhenViewIsLoadedTableViewDelegateIsSetup() {
        XCTAssertTrue(sut.tableView.delegate is DataProvider)
    }
    
    func testWhenViewIsLoadedTableViewDataSourceIsSetup() {
        XCTAssertTrue(sut.tableView.dataSource is DataProvider)
    }
    
    func testWhenViewIsLoadedTableViewDelegateAndDataSourceEquals() {
        XCTAssertEqual(
                sut.tableView.delegate as? DataProvider,
                sut.tableView.dataSource as? DataProvider
        )
    }
    
    func testTaskLstVCHasAddBarButtonWithSelfAsTarget() {
        let target = sut.navigationItem.rightBarButtonItem?.target
        XCTAssertEqual((target as? TaskListViewController), sut)
    }
    
    func performPresentNewTaskViewController() -> NewTaskViewController {
        guard
            let newTaskButton = sut.navigationItem.rightBarButtonItem,
            let action = newTaskButton.action else {
                return NewTaskViewController()
        }
        UIApplication.shared.keyWindow?.rootViewController = sut // Its need in the tests! for go throuht windows hierarhy
        sut.performSelector(onMainThread: action, with: newTaskButton, waitUntilDone: true)
        
        let newTaskViewController = sut.presentedViewController as! NewTaskViewController
        return newTaskViewController
    }
    
    func testAddNewTaskPresentsNewTaskViewController() {
        
        let newTaskViewController = performPresentNewTaskViewController()
        
        XCTAssertNotNil(newTaskViewController.titleTextField)
    }
    
    func testSharedSameTaskManagerWithNewTaskVC() {
        XCTAssertNil(sut.presentedViewController)
        
        let newTaskViewController = performPresentNewTaskViewController()
        
        XCTAssertNotNil(sut.dataProvider.taskManager)
        XCTAssertTrue(newTaskViewController.taskManager === sut.dataProvider.taskManager)
    }
    
    func testWhenViewAppearTableViewReloadData() {
        let mockTableView = MockTableView()
        sut.tableView = mockTableView
        
        sut.beginAppearanceTransition(true, animated: true)
        sut.endAppearanceTransition()
        
        XCTAssertTrue((sut.tableView as! MockTableView).isReloaded)
    }
    
    // this test require clear first section
    func testTappingCellSendsNotification() {
        let task = Task(title: "Foo")
        sut.dataProvider.taskManager!.add(task: task)
        
        expectation(forNotification: NSNotification.Name(rawValue: "DidSelectRow notification"), object: nil) { notification -> Bool in
            
            guard let taskFromNoification = notification.userInfo?["task"] as? Task else { return false }
            return task == taskFromNoification
        }
        
        let tableView = sut.tableView
        tableView?.delegate?.tableView!(tableView!, didSelectRowAt: IndexPath(row: 0, section: 0))
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func testSelectedCellPushesDetailVC() {
        let mockNavigationController = MockNavigationController(rootViewController: sut)
        UIApplication.shared.keyWindow?.rootViewController = mockNavigationController
        
        sut.loadViewIfNeeded()
        
        let task1 = Task(title: "Foo")
        let task2 = Task(title: "Bar")
        
        sut.dataProvider.taskManager!.add(task: task1)
        sut.dataProvider.taskManager!.add(task: task2)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DidSelectRow notification"), object: self, userInfo: ["task": task2])
        
        guard let detailViewController = mockNavigationController.pushedVC as? DetailViewController else {
            XCTFail()
            return
        }
        
        detailViewController.loadViewIfNeeded()
        
        XCTAssertNotNil(detailViewController.titleLabel)
        XCTAssertTrue(detailViewController.task == task2)
    }
}

extension TaskListViewControllerTests {
    class MockTableView: UITableView {
        
        var isReloaded = false
        
        override func reloadData() {
            isReloaded = true
        }
    }
}

extension TaskListViewControllerTests {
    class MockNavigationController: UINavigationController {
        
        var pushedVC: UIViewController?
        
        override func pushViewController(_ viewController: UIViewController, animated: Bool) {
            pushedVC = viewController
            super.pushViewController(viewController, animated: animated)
        }
    }
}
