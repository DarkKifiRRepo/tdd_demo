//
//  DetailViewControllerTests.swift
//  UnitTesting2DemoTests
//
//  Created by Александр Евсеев on 05/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest
import CoreLocation
@testable import UnitTesting2Demo

class DetailViewControllerTests: XCTestCase {

    var sut: DetailViewController!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyboard.instantiateViewController(withIdentifier: String(describing: DetailViewController.self)) as? DetailViewController
        sut.loadViewIfNeeded()
    }

    override func tearDown() {
        
        super.tearDown()
    }
    
    func testControllerHasTitlelabel() {
        
        XCTAssertNotNil(sut.titleLabel)
        XCTAssertTrue(sut.titleLabel.isDescendant(of: sut.view))
    }
    
    func testControllerHasDescriptionlabel() {
        
        XCTAssertNotNil(sut.descriptionLabel)
        XCTAssertTrue(sut.descriptionLabel.isDescendant(of: sut.view))
    }
    
    func testControllerHasDatelabel() {
        
        XCTAssertNotNil(sut.dateLabel)
        XCTAssertTrue(sut.dateLabel.isDescendant(of: sut.view))
    }
    
    func testControllerHasLocationlabel() {
        
        XCTAssertNotNil(sut.locationLabel)
        XCTAssertTrue(sut.locationLabel.isDescendant(of: sut.view))
    }
    
    func testControllerHasMapView() {
        
        XCTAssertNotNil(sut.mapView)
        XCTAssertTrue(sut.mapView.isDescendant(of: sut.view))
    }
    
    func configureControllerForTests() {
        let locationCoor = CLLocationCoordinate2D(latitude: 55.4792046, longitude: 37.3273304)
        let location = Location(named: "Baz", coordinates: locationCoor)
        let date = Date(timeIntervalSince1970: 1546300800)
        let task = Task(title: "Foo", description: "Bar", date: date, location: location)
        sut.task = task
        
        sut.beginAppearanceTransition(true, animated: true) // - equal viewWillAppear
        sut.endAppearanceTransition() // - equal viewDidAppear
    }
    
    func testSettingTaskSetsTitleLabel() {
        configureControllerForTests()
        
        XCTAssertEqual(sut.titleLabel.text, "Foo")
    }
    
    func testSettingTaskSetsDescriptionLabel() {
        configureControllerForTests()
        
        XCTAssertEqual(sut.descriptionLabel.text, "Bar")
    }
    
    func testSettingTaskSetsDateLabel() {
        let df = DateFormatter()
        df.dateFormat = "dd.MM.yy"
        configureControllerForTests()
        
        XCTAssertEqual(sut.dateLabel.text, "01.01.19")
    }
    
    func testSettingTaskSetsLocationLabel() {
        configureControllerForTests()
        
        XCTAssertEqual(sut.locationLabel.text, "Baz")
    }
    
    func testSettingTaskSetsMapView() {
        configureControllerForTests()
        
        XCTAssertEqual(sut.mapView.centerCoordinate.latitude,
                       55.4792046, accuracy: 0.001)
        XCTAssertEqual(sut.mapView.centerCoordinate.longitude,
                       37.3273304, accuracy: 0.001)
    }
}
