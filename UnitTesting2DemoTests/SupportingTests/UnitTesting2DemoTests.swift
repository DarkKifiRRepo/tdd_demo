//
//  UnitTesting2DemoTests.swift
//  UnitTesting2DemoTests
//
//  Created by Александр Евсеев on 03/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest
@testable import UnitTesting2Demo

class UnitTesting2DemoTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testInitialViewControllerIsTaskListViewController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        let rootViewController = navigationController.viewControllers.first as! TaskListViewController
        
        XCTAssertTrue(rootViewController is TaskListViewController) // 'is' test is always true - its a bug of XCode
    }
}
