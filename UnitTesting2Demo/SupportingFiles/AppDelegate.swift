//
//  AppDelegate.swift
//  UnitTesting2Demo
//
//  Created by Александр Евсеев on 03/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        #if targetEnvironment(simulator)
        if CommandLine.arguments.contains("--UITesting") {
//            resetState()
        }
        #endif
        
        return true
    }
    
    private func resetState() {
        guard let documentPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first,
              let url = URL(string: "\(documentPath)tasks.plist")  else { return }
        let fileManager = FileManager.default
        try? fileManager.removeItem(at: url)
    }
}

