//
//  String+Extentions.swift
//  UnitTesting2Demo
//
//  Created by Александр Евсеев on 06/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

extension String {

    var percentEncodingExample: String {
        let allowedCharacters = CharacterSet(charactersIn: "!@#$%^&*()-=\\}{,.[]/?><").inverted
        guard let encodedString = self.addingPercentEncoding(withAllowedCharacters: allowedCharacters) else { fatalError() }
        return encodedString
    }
}
