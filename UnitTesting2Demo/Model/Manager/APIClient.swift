//
//  APIClient.swift
//  UnitTesting2Demo
//
//  Created by Александр Евсеев on 06/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case emptyData
}

protocol URLSessionProtocol {
    func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask
}

extension URLSession: URLSessionProtocol { }

class APIClient {
    
    lazy var urlSession: URLSessionProtocol = URLSession.shared
    
    
    func login(withName name: String, password: String, completionHandler: @escaping (String?, Error?) -> Void) {
        
        let allowedCharacter = CharacterSet.urlQueryAllowed
        guard
            let name = name.addingPercentEncoding(withAllowedCharacters: allowedCharacter),
            let password = password.addingPercentEncoding(withAllowedCharacters: allowedCharacter) else { fatalError() }
        let query = "name=\(name)&password=\(password)"
        guard let url = URL(string: "https://todoapp.com/login?\(query)") else {fatalError() }
        
        urlSession.dataTask(with: url) { (data, responce, error) in
            do {
                guard error == nil else {
                    // catch responce error
                    completionHandler(nil, error)
                    return
                }
                
                guard let data = data else {
                    completionHandler(nil, NetworkError.emptyData)
                    return }
                let dictionary = try JSONSerialization.jsonObject(with: data, options: []) as! [String: String]
                let token = dictionary["token"]
                completionHandler(token, nil)
            } catch {
                // catch error
                completionHandler(nil, error)
            }
        }.resume()
    }
}
