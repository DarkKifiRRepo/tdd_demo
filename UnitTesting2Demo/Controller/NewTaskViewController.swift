//
//  NewTaskViewController.swift
//  UnitTesting2Demo
//
//  Created by Александр Евсеев on 05/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import UIKit
import CoreLocation

class NewTaskViewController: UIViewController {
    @IBOutlet var titleTextField: UITextField!
    @IBOutlet var descriptionTextField: UITextField!
    @IBOutlet var locationTextField: UITextField!
    @IBOutlet var addressTextField: UITextField!
    @IBOutlet var dateTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    var geocoder = CLGeocoder()
    var taskManager: TaskManager!
    var dateFormatter: DateFormatter {
        let df = DateFormatter()
        df.dateFormat = "dd.MM.yy"
        return df
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        save()
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func save() {
        let titleString = titleTextField.text
        let locationString = locationTextField.text
        let descriptionString = descriptionTextField.text
        let date = dateFormatter.date(from: dateTextField.text!)
        let addressString = addressTextField.text
        
        geocoder.geocodeAddressString(addressString!) { [unowned self] (placemarks, error) in
            let placemark = placemarks?.first
            let coordinate = placemark?.location?.coordinate
            let location = Location(named: locationString!, coordinates: coordinate)
            let task = Task(title: titleString!,
                description: descriptionString!,
                date: date,
                location: location)
            self.taskManager.add(task: task)
            
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
