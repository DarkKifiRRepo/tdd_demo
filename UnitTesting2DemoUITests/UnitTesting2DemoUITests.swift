//
//  UnitTesting2DemoUITests.swift
//  UnitTesting2DemoUITests
//
//  Created by Александр Евсеев on 03/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest

class UnitTesting2DemoUITests: XCTestCase {

    var app: XCUIApplication!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app = XCUIApplication()
        app.launchArguments.append("--UITesting")
        app.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func fillTheViews() {
        app.textFields["title text"].tap()
        app.textFields["title text"].typeText("Foo")
        app.textFields["description text"].tap()
        app.textFields["description text"].typeText("Bar")
        app.textFields["location text"].tap()
        app.textFields["location text"].typeText("Baz")
        app.textFields["address text"].tap()
        app.textFields["address text"].typeText("Moscow")
        app.textFields["date text"].tap()
        app.textFields["date text"].typeText("01.01.19")
    }

    func testAddingNewTask() {
        
        XCTAssertTrue(app.isOnMainView)
        
        app.navigationBars["UnitTesting2Demo.TaskListView"].buttons["Add"].tap()
        fillTheViews()
        
        XCTAssertFalse(app.isOnMainView)
        
        app.buttons["Save"].tap()
        
        XCTAssertTrue(app.tables.staticTexts["Foo"].exists)
        XCTAssertTrue(app.tables.staticTexts["01.01.19"].exists)
        XCTAssertTrue(app.tables.staticTexts["Baz"].exists)
    }
    
    func testWhenCellIsSwipedDoneButtonAppeared() {
        
        app.navigationBars["UnitTesting2Demo.TaskListView"].buttons["Add"].tap()
        fillTheViews()
        app.buttons["Save"].tap()
        
        let tableQuery = app.tables.cells
        tableQuery.element(boundBy: 0).swipeLeft()
        tableQuery.element(boundBy: 0).buttons["Done"].tap()
        
        XCTAssertEqual(app.staticTexts.element(matching: .any, identifier: "date").label, "")
    }
}

extension XCUIApplication {
    var isOnMainView: Bool {
        return otherElements["mainView"].exists
    }
}
